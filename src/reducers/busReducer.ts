import BusActionTypes, { DELETE_BUS, EDIT_BUS, GET_BUSES, POST_BUS } from "../actions/BusActions/busActionTypes";
import { IBus } from "../models/models";

interface IBusState {
    buses?: IBus[];
}

const defaultBusState : IBusState ={}

const busReducer = (state: IBusState = defaultBusState, action: BusActionTypes) : IBusState =>{
    let buses: IBus[];
    switch (action.type){
        case GET_BUSES:
            return {buses: action.buses};
        case POST_BUS:
            buses = state.buses ? [ ...state?.buses] : [];
            buses.unshift(action.busData);
            return {buses: buses};
        case EDIT_BUS:
            buses = state.buses ? [ ...state?.buses] : [];
            let ind = buses.findIndex(val=>val._id === action.busId);
            buses[ind] = action.busData;
            return {buses: buses};
        case DELETE_BUS:
            buses = state.buses ? [ ...state?.buses] : [];
            buses.splice(buses.findIndex(val=>val._id === action.busId), 1);
            if(buses.length == 0){
                return {buses: undefined};
            }
            return {buses: buses};
        default: 
            return state;            
    }
}

export default busReducer;