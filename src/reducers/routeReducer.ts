import RouteActionTypes, {
  DELETE_ROUTE,
  DELETE_ROUTE_FROM_BUSID,
  EDIT_ROUTE,
  GET_ROUTES,
  POST_ROUTE,
} from "../actions/RouteActions/routeActionTypes";
import { IRoute } from "../models/models";

interface IRouteState {
  routes?: IRoute[];
}

const defaultRouteState: IRouteState = {};

const routeReducer = (state: IRouteState = defaultRouteState, action: RouteActionTypes): IRouteState => {
  let routes: IRoute[];
  switch (action.type) {
    case GET_ROUTES:
      return { routes: action.routes };
    case POST_ROUTE:
      routes = state.routes ? [...state?.routes] : [];
      routes.unshift(action.routeData);
      return { routes: routes };
    case EDIT_ROUTE:
      routes = state.routes ? [...state?.routes] : [];
      let ind = routes.findIndex((val) => val._id === action.routeId);
      routes[ind] = action.routeData;
      return { routes: routes };
    case DELETE_ROUTE:
      routes = state.routes ? [...state?.routes] : [];
      routes.splice(
        routes.findIndex((val) => val._id === action.routeId),
        1,
      );
      if (routes.length === 0) {
        return { routes: undefined };
      }
      return { routes: routes };
    case DELETE_ROUTE_FROM_BUSID:
      console.log("deleteing routes with busId")
      routes = state.routes ? [...state?.routes] : [];
      routes = routes.filter((route)=>route.bus!==action.busId);
      if(routes.length === 0) {
        return { routes: undefined };
      }
      return { routes: routes };
    default:
      return state;
  }
};

export default routeReducer;
