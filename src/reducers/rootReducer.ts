import { combineReducers } from "redux";
import busReducer from "./busReducer";
import commonReducer from "./commonReducer";
import routeReducer from "./routeReducer";

const rootReducer = combineReducers({
    bus: busReducer,
    route: routeReducer,
    common: commonReducer,
});

export default rootReducer;
