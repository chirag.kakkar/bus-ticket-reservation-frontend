import React, { useEffect, useState } from "react";
import { Redirect, Route, Switch } from "react-router";
import "./App.css";
import BusesPage from "./containers/BusesPage/BusesPage";
import NewBooking from "./containers/NewBooking/NewBooking";
import RoutesPage from "./containers/RoutesPage/RoutesPage";
import EditBus from "./components/EditBus/EditBus";
import { EmptyBus, isSignedIn } from "./utils";
import busActions from "./actions/BusActions/busActions";
import { BoundThunk } from "./utils";
import { connect } from "react-redux";
import axios from "./axiosInstance";
import SinginPage from "./containers/SigninPage/SigninPage";
import AdminBookingsPage from "./containers/AdminBookingsPage/AdminBookingsPage";
import commonActions from "./actions/CommonActions/commonActions";

const mapDispatchToProps = { ...busActions, ...commonActions };
type IDispatchProps = { [P in keyof typeof mapDispatchToProps]: BoundThunk<typeof mapDispatchToProps[P]> };
type IProps = IDispatchProps;

const App: React.FunctionComponent<IProps> = (props: IProps) => {
  axios.defaults.headers["x-access-token"] = window.localStorage.getItem("x-access-token") as string;

  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    console.log("app mounted");
    isSignedIn()
      .then((value) => {
        if (value) {
          props.setAuth(true, window.localStorage.getItem("x-access-token") as string)
          .then((val) => setIsLoading(false));
        } else {
          setIsLoading(false);
        }
      })
      .catch((err) => setIsLoading(false));
    // (window as any).$('#toastid').toast('show');
  }, []);

  return (
    <div className="App h-100">
      {isLoading ? (
        <div
          className="modal d-block"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabIndex={-1}
          aria-labelledby="staticBackdropLabel"
        >
          <div className="d-flex justify-content-center h-100 align-items-center">
            <div className="spinner-border" role="status">
              <span className="visually-hidden">Loading...</span>
            </div>
          </div>
        </div>
      ) : (
        <>
          <Switch>
            <Route path="/signin" exact component={SinginPage} />
            <Route path="/buses" exact component={BusesPage} />
            {/* <Route path="/buses/add" exact component={()=><EditBus {...EmptyBus} isNew={true} postBus={props.postBus} editBus={props.editBus}/>}/> */}
            <Route path="/routes" exact component={RoutesPage} />
            <Route path="/manage-bookings" exact component={AdminBookingsPage} />
            <Route path="/" exact component={NewBooking} />
            <Route path="/" render={() => <Redirect to="/" />} />
          </Switch>
        </>
      )}
    </div>
  );
};

export default connect(null, mapDispatchToProps)(App);
