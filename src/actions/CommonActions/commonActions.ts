import axios from "../../axiosInstance";
import { Dispatch } from "redux";
import { ICombinedState } from "../../store";
import ActionTypes from "../actionTypes";
import { AUTH } from "./commonActionTypes";


export const setAuth = (isLoggedIn: boolean, token?:string) => async (dispatch: Dispatch<ActionTypes>, getState: ()=>ICombinedState) => {
    console.log("reached setAuth");
    if(!isLoggedIn){
        window.localStorage.removeItem('x-access-token');
        axios.defaults.headers['x-access-token'] = null;
    }
    else{
        window.localStorage.setItem('x-access-token', token as string);
        axios.defaults.headers['x-access-token'] = token;
    }
    dispatch({type: AUTH, isLoggedIn: isLoggedIn});
}

export default {
    setAuth,
}
