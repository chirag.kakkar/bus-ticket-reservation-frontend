import { IBus, IRoute } from "../../models/models";

export const GET_ROUTES = "GET_ROUTES";
export const POST_ROUTE = "POST_ROUTE";
export const DELETE_ROUTE = "DELETE_ROUTE";
export const EDIT_ROUTE = "EDIT_ROUTE";
export const DELETE_ROUTE_FROM_BUSID = "DELETE_ROUTE_FROM_BUSID";

export interface IGetRoutesAction {
    type: typeof GET_ROUTES;
    routes: IRoute[];
}
export interface IPostRouteAction {
    type: typeof POST_ROUTE;
    routeData: IRoute;
}
export interface IDeleteRouteAction {
    type: typeof DELETE_ROUTE;
    routeId: IRoute["_id"];
}
export interface IEditRouteAction {
    type: typeof EDIT_ROUTE;
    routeData: IRoute;
    routeId: IRoute["_id"];
}
export interface IDeleteRouteFromBusId {
    type: typeof DELETE_ROUTE_FROM_BUSID;
    busId: IBus["_id"];
}

type RouteActionTypes =  IGetRoutesAction | IPostRouteAction | IDeleteRouteAction | IEditRouteAction | IDeleteRouteFromBusId;
export default RouteActionTypes;