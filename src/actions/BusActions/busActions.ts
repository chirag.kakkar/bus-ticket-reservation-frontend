import axios from "../../axiosInstance";
import { Dispatch } from "redux";
import ActionTypes from "../actionTypes";
import { LOADING } from "../CommonActions/commonActionTypes";
import { IBus } from "../../models/models";
import { DELETE_BUS, EDIT_BUS, GET_BUSES, POST_BUS } from "./busActionTypes";
import { showError } from "../../utils";
import { ICombinedState } from "../../store";
import { DELETE_ROUTE_FROM_BUSID } from "../RouteActions/routeActionTypes";

export const getBuses = () => async (dispatch: Dispatch<ActionTypes>, getState: ()=>ICombinedState) => {
    dispatch({
        type: LOADING,
        isLoading: true,
    });

    try {
        const {data, status} = await axios.get<IBus[]>('/bus');
        dispatch({type: GET_BUSES, buses: data});
    } catch (error) {
        showError("Error fetching buses", dispatch);
    }

    dispatch({type: LOADING, isLoading: false})
}

export const postBus = (newBus: IBus) => async (dispatch: Dispatch<ActionTypes>) => {
    try {
        const {data, status} = await axios.post<IBus>('/bus', newBus);
        dispatch({type: POST_BUS, busData: data});
    } catch (error) {
        showError("Error creating new bus", dispatch);
    }
}

export const editBus = (busData: IBus, busId: IBus["_id"]) => async (dispatch: Dispatch<ActionTypes>) => {
    delete busData._id;
    try {
        const {data, status} = await axios.put<IBus>(`/bus/${busId}`, busData);
        dispatch({type: EDIT_BUS, busData: data, busId: busId});
    } catch (error) {
        showError("Error updating bus", dispatch);
    }
}

export const deleteBus = (busId: IBus["_id"]) => async (dispatch: Dispatch<ActionTypes>) => {
    try {
        const {data, status} = await axios.delete(`/bus/${busId}`);
        dispatch({type: DELETE_BUS, busId: busId});
        dispatch({type:DELETE_ROUTE_FROM_BUSID, busId: busId});
    } catch (error) {
        showError("Error deleting bus", dispatch);
    }
}

export default {
    getBuses,
    deleteBus,
    editBus,
    postBus,
}