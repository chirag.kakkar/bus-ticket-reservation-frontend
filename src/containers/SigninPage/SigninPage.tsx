import React from "react";
import { RouteComponentProps } from "react-router-dom";
import commonActions from "../../actions/CommonActions/commonActions";
import axios from "../../axiosInstance";
import withNavbarHoc from "../../hoc/NavbarHoc/NavbarHoc";
import { ErrorContext, IMessage, SuccessContext } from "../../models/models";
import { ICombinedState } from "../../store";
import { BoundThunk, showError } from "../../utils";

interface ISigninProps {}
interface ISiginState {
  message: IMessage | null;
}

const mapStateToProps = (state: ICombinedState) => ({
  isLoggedIn: state.common.isLoggedIn,
});

const mapDispatchToProps = commonActions;
type IDispatchProps = { [P in keyof typeof mapDispatchToProps]: BoundThunk<typeof mapDispatchToProps[P]> };

type IProps = ISigninProps & RouteComponentProps & IDispatchProps & ReturnType<typeof mapStateToProps>;

class SinginPage extends React.Component<IProps, ISiginState> {
  state: ISiginState = {
    message: null,
  };

  setMessage = async (message: IMessage | null) => {
    this.setState((prevState) => ({
      message: message,
    }));
  };

  onFormSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    this.setMessage(null);

    const target = e.target as typeof e.target & {
      email: { value: string };
      password: { value: string };
    };
    const signinData = {
      email: target.email.value,
      password: target.password.value,
    };
    try {
      const { data } = await axios.post("/admin/signin", signinData);
      // window.localStorage.setItem("x-access-token", data.accessToken);
      // axios.defaults.headers["x-access-token"] = data.accessToken;
      this.props.setAuth(true, data.accessToken);
      this.props.history.push("/");
    } catch (error) {
      this.setMessage({text: "Some error occured", context: ErrorContext});
    }
  };

  render() {
    return (
      <div className="container my-auto">
        <form onSubmit={this.onFormSubmit}>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input className="form-control" type="email" name="email" id="email" placeholder="Email" required/>
          </div>

          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input type="password" className="form-control" name="password" id="password" required />
          </div>
          <div className="form-group">
            <input type="submit" className="form-control btn btn-primary" value={"Signin"}/>
          </div>
        </form>
        {this.state.message && (
          <div
            className={`row alert ${
              this.state.message.context === ErrorContext
                ? "alert-danger"
                : this.state.message.context === SuccessContext
                ? "alert-success"
                : "alert-info"
            }`}
          >
            {this.state.message.text}
          </div>
        )}
      </div>
    );
  }
}

export default withNavbarHoc(SinginPage);
