import React, { useEffect, useState } from "react";
import { IBus } from "../../models/models";
import * as busTypes from "../../models/BusTypes";
import styles from "./EditBus.module.css";

interface IProps extends Omit<IBus, "_id"> {
  _id?: string;
  isNew: boolean;
  postBus: (newBus: IBus) => any;
  editBus: (busData: IBus, busId: IBus["_id"]) => any;
}

const EditBus: React.FunctionComponent<IProps> = (props) => {
  const [vehicleNumber, setVehicleNumber] = useState<string>("");
  const [numOfSeats, setNumOfSeats] = useState<string>("40");
  const [vehicleMake, setVehicleMake] = useState<string>("");
  const [vehicleModel, setVehicleModel] = useState<string>("");
  //   const [images, setImages] = useState<string[]>([]);
  const [busType, setBusType] = useState<string>(Object.values(busTypes)[0]);

  useEffect(() => {
    if (!props.isNew) {
      setVehicleNumber(props.vehicleNumber);
      setNumOfSeats("" + props.numOfSeats);
      setVehicleMake(props.vehicleMake);
      setVehicleModel(props.vehicleModel);
      setBusType(props.busType);
      // setImages(images);
    }
  }, [props.isNew]);

  const onFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const busData: IBus = {
      vehicleNumber: vehicleNumber,
      vehicleMake: vehicleMake,
      vehicleModel: vehicleModel,
      busType: busType,
      images: [],
      numOfSeats: parseInt(numOfSeats),
    };
    console.log("seats count", busData.numOfSeats);
    if (props.isNew) {
      props.postBus(busData);
    } else {
      props.editBus(busData, props._id as string);
    }
    (window as any).$(`#modal${props._id}`).modal("hide");
  };

  return (
    <div className="container">
      <div className="row">
        <form onSubmit={onFormSubmit}>
          <div className="form-group">
            <div className="d-flex align-items-center mb-3">
              <i className={`large material-icons ${styles.icon}`}>confirmation_number</i>
              <div className="form-floating flex-grow-1">
                <input
                  type="text"
                  className="form-control"
                  id="vehicleNumber"
                  disabled={!props.isNew}
                  value={vehicleNumber}
                  onChange={(e) => setVehicleNumber(e.target.value)}
                  placeholder="CG09XXXX"
                />
                <label htmlFor="vehicleNumber">Vehicle Number</label>
              </div>
            </div>
          </div>
          <div className="row mb-3">
            <div className="col col-md-6 d-flex align-items-center">
              <i className={`large material-icons ${styles.icon}`}>airport_shuttle</i>
              <div className="form-floating flex-grow-1">
                <input
                  type="text"
                  className="form-control"
                  id="vehicleMake"
                  value={vehicleMake}
                  onChange={(e) => setVehicleMake(e.target.value)}
                  placeholder="Tata"
                />
                <label htmlFor="vehicleMake">Vehicle Make</label>
              </div>
            </div>
            <div className="col col-md-6 d-flex align-items-center">
              <i className={`large material-icons ${styles.icon}`}>directions_bus</i>
              <div className="form-floating flex-grow-1">
                <input
                  type="text"
                  className="form-control"
                  id="vehicleModel"
                  value={vehicleModel}
                  onChange={(e) => setVehicleModel(e.target.value)}
                  placeholder="Indica XX"
                />
                <label htmlFor="vehicleModel">Vehicle Model</label>
              </div>
            </div>
          </div>
          <div className="row mb-3">
            <div className="col col-md-6 d-flex align-items-center">
              <i className={`large material-icons ${styles.icon}`}>airline_seat_recline_extra</i>
              <div className="form-floating flex-grow-1">
                <select
                  id="busType"
                  className="form-control"
                  placeholder="Bus Type"
                  value={busType}
                  onChange={(e) => setBusType(e.target.value)}
                >
                  {Object.values(busTypes).map((value) => (
                    <option key={value} value={value}>
                      {value}
                    </option>
                  ))}
                </select>
                <label htmlFor="busType">Bus Type</label>
              </div>
            </div>
            <div className="col col-md-6 d-flex align-items-center">
              <i className={`large material-icons ${styles.icon}`}>filter_none</i>
              <div className="form-floating flex-grow-1">
                <input
                  type="number"
                  min={40}
                  step={1}
                  className="form-control"
                  id="numOfSeats"
                  disabled={!props.isNew}
                  value={numOfSeats}
                  onChange={(e) => setNumOfSeats(e.target.value)}
                  placeholder="4X"
                />
                <label htmlFor="numOfSeats">Seats count</label>
              </div>
            </div>
          </div>
          <div className="form-group text-center mb-3">
            <input type="submit" className="btn btn-primary" value={props.isNew ? "Create" : "Update"} />
          </div>
        </form>
      </div>
    </div>
  );
};

export default EditBus;
