import React, { useState } from "react";
import Seats from "../Seats/Seats";
import styles from "./RouteWrapperCard.module.css";
import {
  ErrorContext,
  IMessage,
  IPersonDetails,
  IReservation,
  IReservationDetails,
  IReservedSeats,
  IRoute,
  IRouteWrapper,
  SuccessContext,
} from "../../models/models";
import { formatDate, toMonthStr } from "../../utils";
import axios from "../../axiosInstance";
import ReservationDetails from "../../components/ReservationDetails/ReservationDetails";

interface Iprops extends IRouteWrapper {
  addReservation?: (routeId: string, reservation: IReservation) => Promise<void>;
  cancelReservation?: (routeId: string, reservationId: string) => Promise<void>;
  isAdmin: boolean;
}

const RouteWrapperCard: React.FunctionComponent<Iprops> = (props) => {
  const [showSeats, setShowSeats] = useState(false);
  const [selectedSeat, setSelectedSeat] = useState<number | null>(null);
  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [mobile, setMobile] = useState<string>("");
  const [age, setAge] = useState<string>("");
  const [gender, setGender] = useState<"" | "M" | "F">("");
  const [message, setMessage] = useState<IMessage | null>(null);

  const setSelectedSeatWrapper = (seatNumber: number) => {
    setMessage(null);
    if (seatNumber == selectedSeat) {
      setSelectedSeat(null);
    } else {
      setSelectedSeat(seatNumber);
    }
  };

  const cancelReservation = async (reservationId: string) => {
    console.log("cancellng", reservationId, props._id, props.travelDate);
    try {
      await axios.delete(`/booking/${reservationId}`);
      setMessage({ text: "Reservation deleted successfully", context: SuccessContext });
      if (props.cancelReservation) {
        props.cancelReservation(props._id as string, reservationId);
      }
    } catch (err) {
      console.log("some error occured");
    }
  };

  const onBookFormSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (selectedSeat === null) {
      setMessage({ text: "Plese select a seat first", context: ErrorContext });
    } else {
      const personDetails: IPersonDetails = {
        name: name,
        email: email,
        mobile: mobile,
      };
      if (age !== "") {
        personDetails.age = parseInt(age);
      }
      if (gender != "") {
        personDetails.gender = gender;
      }
      try {
        const { data, status } = await axios.post<IReservation>("/booking", {
          travelDate: formatDate(props.travelDate),
          route: props._id,
          personDetails: personDetails,
          seatNumber: selectedSeat,
        });
        setMessage({ text: `Seat number ${selectedSeat} reserved successfully`, context: SuccessContext });
        setSelectedSeat(null);
        // console.log(data);
        if (!props.isAdmin && props.addReservation) {
          props.addReservation(props._id, data);
        }
      } catch (error) {
        if (error?.response?.data?.err) {
          console.log("setting error message");
          setMessage({ text: error.response.data.err as string, context: ErrorContext });
        } else if (error?.response?.data?.msg) {
          setMessage({ text: error.response.data.msg as string, context: ErrorContext });
        } else {
          console.log("setting error message");
          setMessage({ text: "Some error occured", context: ErrorContext });
        }
      }

      // console.log("clicked");
    }
  };

  let travelDurationTime = new Date(props.travelDuration);
  const departureDate = new Date();
  departureDate.setHours(props.departureHour);
  departureDate.setMinutes(props.departureMinute);
  const durationHour = travelDurationTime.getHours();
  const durationMinute = travelDurationTime.getMinutes();
  const arrivalDate = new Date(departureDate.getTime() + props.travelDuration);
  const arrivalHour = arrivalDate.getHours();
  const arrivalMinute = arrivalDate.getMinutes();
  let reservedSeats: IReservedSeats = {};
  for (let res of props.reservations) {
    reservedSeats[res.seatNumber] = true;
  }
  let reservationDetails: IReservationDetails = {};
  if (props.isAdmin) {
    for (let res of props.reservations) {
      reservationDetails[res.seatNumber] = res;
    }
  }
  // console.log(reservationDetails);
  return (
    <div className={`${styles.topContainer} container my-3`}>
      <div className="row">
        <div className="col col-9 p-3">
          <div>
            <span className={`${styles.vehicleModel}`}>{props.bus.vehicleModel}</span>
            <span className={`${styles.vehicleNumber}`}>{props.bus.vehicleNumber}</span>
          </div>
          <div>
            <span className={`${styles.busType}`}>{props.bus.busType}</span>|
            <span className={`${styles.numOfSeats}`}>
              {props.bus.numOfSeats - props.reservations.length} seats left
            </span>
          </div>
          <div className="d-flex justify-content-start align-items-center">
            <div>
              <span className={`${styles.timeBold}`}>
                {props.departureHour.toString().padStart(2,"0")}:{props.departureMinute.toString().padStart(2,"0")}
              </span>
              <span className={`${styles.timeLight}`}>
                {" " + departureDate.getDate()}
                {" " + toMonthStr(departureDate.getMonth())}
              </span>
            </div>
            <div className={`${styles.timeLine}`}></div>
            <div>
              <span className={`${styles.timeBold}`}>{durationHour.toString().padStart(2,"0")}</span>
              <span className={`${styles.timeLight} pr-1`}>hrs </span>
              <span className={`${styles.timeBold}`}>{durationMinute.toString().padStart(2,"0")}</span>
              <span className={`${styles.timeLight}`}>mins</span>
            </div>
            <div className={`${styles.timeLine}`}></div>
            <div>
              <span className={`${styles.timeBold}`}>
                {arrivalHour.toString().padStart(2,"0")}:{arrivalMinute.toString().padStart(2,"0")}
              </span>
              <span className={`${styles.timeLight}`}>
                {" " + arrivalDate.getDate()}
                {" " + toMonthStr(arrivalDate.getMonth())}
              </span>
            </div>
          </div>
        </div>
        <div className="col col-3 p-2 text-center">
          <h1><span>₹</span>{props.price}</h1>
          <button onClick={() => setShowSeats(!showSeats)}>{showSeats ? "Hide seats" : "Show seats"}</button>
        </div>
      </div>
      {showSeats && (
        <div className="row d-flex justify-content-center">
          <div className="col col-sm-12 ">
            <Seats
              isAdmin={props.isAdmin}
              numOfSeats={props.bus.numOfSeats}
              reservedSeats={reservedSeats}
              selectedSeat={selectedSeat}
              setSelectedSeat={setSelectedSeatWrapper}
            />
          </div>
          {props.isAdmin ? (
            <ReservationDetails
              selectedSeat={selectedSeat}
              reservationDetails={reservationDetails}
              cancelReservation={cancelReservation}
            />
          ) : (
            <div className="col col-sm-12 pb-3 pr-3">
              <h6>Enter details</h6>
              <form
                onSubmit={(e) => {
                  setMessage(null);
                  onBookFormSubmit(e);
                }}
              >
                <div className="d-flex align-items-center mb-3">
                  <i className={`large material-icons ${styles.icon}`}>perm_identity</i>
                  <div className="form-floating flex-grow-1">
                    <input
                      required
                      className="form-control"
                      type="text"
                      placeholder="Enter Name"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                    <label>Name</label>
                  </div>
                </div>
                <div className="d-flex align-items-center mb-3">
                  <i className={`large material-icons ${styles.icon}`}>email</i>
                  <div className="form-floating flex-grow-1">
                    <input
                      required
                      className="form-control"
                      type="email"
                      placeholder="Enter email"
                      value={email}
                      aria-label="email"
                      aria-describedby="email-addon"
                      onChange={(e) => setEmail(e.target.value)}
                    />
                    <label>Email</label>
                  </div>
                </div>
                <div className="d-flex align-items-center mb-3">
                  <i className={`large material-icons ${styles.icon}`}>call</i>
                  <div className="form-floating flex-grow-1">
                    <input
                      required
                      className="form-control"
                      type="tel"
                      pattern="\d{10,11}"
                      placeholder="Enter mobile"
                      value={mobile}
                      onChange={(e) => setMobile(e.target.value)}
                    />
                    <label>Mobile</label>
                  </div>
                </div>
                <div className="row mb-3">
                  <div className="col d-flex align-items-center">
                    <i className={`large material-icons ${styles.icon}`}>face</i>
                    <div className="form-floating flex-grow-1">
                      <select
                        className="form-control"
                        placeholder="Gender"
                        value={gender}
                        onChange={(e) => setGender(e.target.value as typeof gender)}
                      >
                        <option value="">Select Gender</option>
                        <option value="F">F</option>
                        <option value="M">M</option>
                      </select>
                      <label>Gender</label>
                    </div>
                  </div>
                  <div className="col d-flex align-items-center">
                    <i className={`large material-icons ${styles.icon}`}>event</i>
                    <div className="form-floating flex-grow-1">
                    <input
                      type="number"
                      min={1}
                      step={1}
                      max={150}
                      pattern="\d*"
                      className="form-control"
                      placeholder="Age"
                      value={age}
                      onChange={(e) => setAge(e.target.value)}
                    />
                    <label>Age</label>
                    </div>
                  </div>
                </div>
                <div className="form-group text-center">
                  <input type="submit" className="btn btn-primary" value="Book now" />
                </div>
              </form>
            </div>
          )}
        </div>
      )}
      {message && (
        <div
          className={`row alert ${
            message.context === ErrorContext
              ? "alert-danger"
              : message.context === SuccessContext
              ? "alert-success"
              : "alert-info"
          }`}
        >
          {message.text}
        </div>
      )}
    </div>
  );
};

export default RouteWrapperCard;
