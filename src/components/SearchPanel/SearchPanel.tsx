import React, { useState } from "react";

interface IProps {
    fetchRoutes:  (from: string, to:string, travelDate: string)=>void;
}

const SearchPanel: React.FunctionComponent<IProps> = (props: React.PropsWithChildren<IProps>) => {
  const findBuses = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const target = e.target as typeof e.target & {
      from: HTMLInputElement;
      to: HTMLInputElement;
      travelDate: HTMLInputElement;
    };
    props.fetchRoutes(target.from.value, target.to.value, target.travelDate.value);
  };

  return (
    <div>
      <form onSubmit={findBuses} className="row g-2 align-items-center">
       <div className="form-floating col-auto ">
        <input type="text" className="form-control" placeholder="Enter from" name="from" />
        <label htmlFor="from">From</label>
       </div>
       <div className="form-floating col-auto ">
        <input type="text" className="form-control" placeholder="Enter to" name="to" />
        <label htmlFor="To">To</label>
       </div>
       <div className="form-floating col-auto ">
        <input type="date" className="form-control" placeholder="Travel Date" name="travelDate"></input>
        <label htmlFor="travelDate">Travel Date</label>
       </div>
       <div className="col-auto">
        <button type="submit" className="btn btn-primary btn-lg">Find</button>
        </div>
      </form>
    </div>
  );
};

export default SearchPanel;
