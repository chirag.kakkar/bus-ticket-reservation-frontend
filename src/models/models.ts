export interface IBus {
  _id?: string;
  vehicleNumber: string;
  numOfSeats: number;
  vehicleMake: string;
  vehicleModel: string;
  images: string[];
  busType: string;
  __v?: number;
}

export interface IReservation {
  _id: string;
  seatNumber: number;
  personDetails?: IPersonDetails;
  updatedAt: string;
  createdAt: string;
}

export interface IRoute {
  _id?: string;
  days: number[];
  from: string;
  to: string;
  departureHour: number;
  departureMinute: number;
  travelDuration: number;
  bus: string;
  price: number;
  __v?: number;
}

export interface IRouteWrapper extends Omit<IRoute, "bus"> {
  _id: string;
  travelDate: string;
  reservations: IReservation[];
  bus: IBus;
}

export interface IPersonDetails {
  name: string;
  mobile: string;
  email: string;
  age?: number;
  gender?: "M" | "F";
}
export const InfoContext =0;
export const SuccessContext =1;
export const ErrorContext =3;

export interface IMessage {
  text: string;
  context: typeof InfoContext | typeof SuccessContext | typeof ErrorContext;
}

export type IReservedSeats = Record<number, boolean>;
export type IReservationDetails = Record<number, IReservation>;

// export interface IReservationWithDetails extends IReservation {
//   personDetails: IPersonDetails;
// }

// export interface IRouteWrapperAdmin extends Omit<IRoute, "bus"> {
//   _id: string;
//   travelDate: string;
//   reservations: IReservationWithDetails[];
//   bus: IBus;
// }