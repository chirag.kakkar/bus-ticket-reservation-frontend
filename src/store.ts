import { createStore, compose, applyMiddleware } from "redux";
import rootReducer from "./reducers/rootReducer";
import thunk, { ThunkMiddleware } from "redux-thunk";
import ActionTypes from "./actions/actionTypes";

const store = createStore(rootReducer, compose(applyMiddleware(thunk as ThunkMiddleware<ICombinedState, ActionTypes>)));

export type ICombinedState = ReturnType<typeof rootReducer> 

export default store;